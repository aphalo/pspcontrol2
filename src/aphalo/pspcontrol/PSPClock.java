package aphalo.pspcontrol;

/*
 * PSPClock.java
 *
 * Created on 6 June 2009
 * Revised on 17-18 June 2013 (added cycling)
 *
 * Time keeping class for the PSPControl application
 * used to control the programable PSU <em>PSP-2010</em> from <em>GW Instek</em>
 * through an RS-232 serial port.
 *
 * usage: java -jar PSPControl.jar [<em>fileName</em>]
 *
 * where <em>fileName</em> is the name of a properties file
 * with fields: comPort, startHour, startMinute, stopHour, stopMinute
 *
 */
/*
 * ********* TO DO ************* 10 June 2009 *** PJA ***
 *
 * lock COM port while sending commands to the PSP
 *
 * improve exception handling when sending commands, some kind of retry could
 * useful
 *
 * to change window title use cmd.exe and TITLE as shown in LEDs.bat
 *
 */
import java.io.*;
import java.io.FileInputStream;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Properties;
import java.util.Timer;
import java.util.TimerTask;
import java.text.SimpleDateFormat;

/**
 * Timed switching ON and OFF of PSP-2010 power supply. Schedule and com port
 * name are supplied in a properties file. The default name of the file is
 * PSP.properties and is used if another name is not supplied as the first
 * argument when calling the jar file.
 *
 * Version 2.0 adds pulsing of output. New properties added, old properties
 * files need to be edited.
 *
 * @version 2.0
 *
 * @author Pedro J. Aphalo, University of Helsinki.
 *
 * Some bits of code adapted from example found in a forum. ORIGINAL AUTHOR:
 * masijade ORIGINAL CODE AT:
 * <a
 * href="http://www.java-forums.org/new-java/11785-sleep-until-certian-time-day.html">
 */
public class PSPClock {

    private String comPort;
    // for day and night
    private int stopHour;
    private int stopMinute;
    private int startHour;
    private int startMinute;
    private boolean isOffAtMidnight;
    // for cycling
    private boolean cyclingFlag;
    private int cycleLength; // one full cycle in seconds
    private float cycleDuty; // fraction of one, e.g. 0.40 for light on for 40% of cycle
    private int cycleON;
    private int cycleOFF;
    // electrical settings (currently ignored)
    private float voltageLimit;
    private float currentLimit;
    private float voltageSettingLow;
    private float voltageSettingHigh;
    private Timer timer = new Timer(true);  // Create just one and make it a daemon thread.

    /**
     * Read properties from file. Values used to initialize state of run
     */
    private void readProperties(String fileNameStr) {
        Properties props = new Properties();

        try {
            FileInputStream propin = new FileInputStream(fileNameStr);
            props.load(propin);
            propin.close();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } /*finally {
         if (propin != null) try { propin.close(); } catch (IOException ioe) {}
         }*/

        if (props.entrySet().size() != 11) {
            System.err.println("Not all Properties Defined. size() =" + props.entrySet().size());
            System.exit(1);
        } else {
            comPort = props.getProperty("comPort");
            stopHour = Integer.valueOf(props.getProperty("stopHour"));
            stopMinute = Integer.valueOf(props.getProperty("stopMinute"));
            startHour = Integer.valueOf(props.getProperty("startHour"));
            startMinute = Integer.valueOf(props.getProperty("startMinute"));
            cycleLength = Integer.valueOf(props.getProperty("cycleLength"));
            cycleDuty = Float.valueOf(props.getProperty("cycleDuty"));
            voltageLimit = Float.valueOf(props.getProperty("voltageLimit"));
            currentLimit = Float.valueOf(props.getProperty("currentLimit"));
            voltageSettingLow = Float.valueOf(props.getProperty("voltageSettingLow"));
            voltageSettingHigh = Float.valueOf(props.getProperty("voltageSettingHigh"));

            if (stopHour < 0 || stopHour > 23) {
                System.out.println("stopHour out of range 0..23");
                System.exit(1);
            }
            if (startHour < 0 || startHour > 23) {
                System.out.println("startHour out of range 0..23");
                System.exit(1);
            }
            if (stopMinute < 0 || stopMinute > 59) {
                System.out.println("stopMinute out of range 0..59");
                System.exit(1);
            }
            if (startMinute < 0 || startMinute > 59) {
                System.out.println("startMinute out of range 0..59");
                System.exit(1);
            }
            isOffAtMidnight = (startHour * 60 + startMinute) < (stopHour * 60 + stopMinute);
            cyclingFlag = cycleLength > 0;
            if (cyclingFlag) {
                if (cycleLength < 2 || cycleLength > (3600 * 4)) {
                    System.out.println("cycleLenght out of range 10.." + (3600 * 4));
                }
                cycleON = (int) (cycleLength * cycleDuty);
                cycleOFF = cycleLength - cycleON;
                if (cycleON < 1 || cycleOFF < 1) {
                    System.out.println("ON or OFF half cycle should last at least 1 second");
                }
            }
        }
    }

    /**
     * logic to check if we are in OFF period or not
     */
    private boolean shouldBeOffNow(int curHour, int curMin) {
        boolean afterStop = (curHour > stopHour) || (curHour == stopHour && curMin >= stopMinute);
        boolean beforeStart = (curHour < startHour) || ((curHour == startHour) && (curMin < startMinute));
        if (isOffAtMidnight) {
            return afterStop || beforeStart;
        } else {
            return afterStop && beforeStart;
        }
    }

    /**
     * Control the PSP relay at the requested times.
     */
    void runPSPControl(String propFileName) {

        System.out.println("-- PSPControl 2.0 --");
        System.out.println(" type '@' to exit");
        System.out.println(" type '$' to toggle keyboard lock");
        Calendar rightNow = Calendar.getInstance();
        SimpleDateFormat formatter = new SimpleDateFormat("E yyyy.MM.dd 'at' HH:mm:ss zzz");
        System.out.println("Started running at: " + formatter.format(rightNow.getTime()));
        System.out.println("Reading properties file '" + propFileName + "'");
        readProperties(propFileName);
        System.out.println("Switch on at  " + startHour + ":" + startMinute);
        System.out.println("Switch off at " + stopHour + ":" + stopMinute);
        if (cyclingFlag) {
            System.out.println("Cycling: ON for " + cycleON + " s and OFF for " + cycleOFF + " s.");
        }
        System.out.println("Opening: " + comPort);
        try {
            (new PSPControl()).connect(comPort);
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        System.out.println("Setting I and V limits, switch OFF, and\nV setting to Low value.");
        PSPControl.prepare();
        PSPControl.outputOff();
        PSPControl.voltageLimitSet(voltageLimit);
        PSPControl.currentLimitSet(currentLimit);
        PSPControl.voltageSet(voltageSettingLow);
        PSPControl.queryState();
        
        Calendar gc = new GregorianCalendar();
        Calendar cycleGc = new GregorianCalendar();
        Calendar cycleStart = new GregorianCalendar();
        Calendar cycleCrossOver = new GregorianCalendar();
        Calendar cycleEnd = new GregorianCalendar();

        while (true) {
            rightNow = Calendar.getInstance();
            System.out.println("Now : " + formatter.format(rightNow.getTime()));

            gc = Calendar.getInstance();
            int currentHour = gc.get(Calendar.HOUR_OF_DAY);
            int currentMinute = gc.get(Calendar.MINUTE);
            int currentSecond = gc.get(Calendar.SECOND);
            int currentMillisecond = gc.get(Calendar.MILLISECOND);

            if (shouldBeOffNow(currentHour, currentMinute)) {
                if (isOffAtMidnight && ((currentHour > stopHour)
                        || (currentHour == stopHour && currentMinute >= stopMinute))) {
                    gc.add(Calendar.DAY_OF_MONTH, 1);
                }
                gc.set(Calendar.HOUR_OF_DAY, startHour);
                gc.set(Calendar.MINUTE, startMinute);
                gc.set(Calendar.SECOND, 0);
                gc.set(Calendar.MILLISECOND, 0);
                System.out.println("Flipping the switch off");
                PSPControl.prepare();
                PSPControl.queryState();
                PSPControl.outputOff();
                PSPControl.queryState();
            } else {
                if (!isOffAtMidnight && ((currentHour > startHour)
                        || (currentHour == startHour && currentMinute >= startMinute))) {
                    gc.add(Calendar.DAY_OF_MONTH, 1);
                }
                gc.set(Calendar.HOUR_OF_DAY, stopHour);
                gc.set(Calendar.MINUTE, stopMinute);
                gc.set(Calendar.SECOND, 0);
                gc.set(Calendar.MILLISECOND, 0);
                
                System.out.println("Setting voltage to " + voltageSettingHigh);
                PSPControl.prepare();
                PSPControl.voltageSet(voltageSettingHigh);

                System.out.println("Flipping the switch on");
                if (cyclingFlag) {
                    int cycleNumber = 1;
                    cycleStart = Calendar.getInstance();
                    cycleCrossOver = (Calendar) cycleStart.clone();
                    cycleEnd = (Calendar) cycleStart.clone();
                    cycleCrossOver.add(Calendar.SECOND, cycleON);
                    cycleEnd.add(Calendar.SECOND, (cycleON + cycleOFF));
                    while (cycleCrossOver.before(gc)) {
                        PSPControl.prepare();
                        if (voltageSettingLow <= 0.0){
                            PSPControl.outputOn();
                        } else {
                            PSPControl.voltageSet(voltageSettingHigh);
                        }
                        System.out.println("Starting ON/High half cycle at " + formatter.format(cycleStart.getTime()));
                        PSPControl.queryState();
                        waitUntil(cycleCrossOver.getTime());
                        PSPControl.prepare();
                        if (voltageSettingLow <= 0.0){
                            PSPControl.outputOff();
                        } else {
                            PSPControl.voltageSet(voltageSettingLow);
                        }
                        System.out.println("Starting OFF/Low half cycle at " + formatter.format(cycleCrossOver.getTime()));
                        PSPControl.queryState();
                        waitUntil(cycleEnd.getTime());
                        System.out.println("End of cycle no. " + cycleNumber + " at " + formatter.format(cycleEnd.getTime()));
                        cycleNumber += 1;
                        cycleStart.add(Calendar.SECOND, cycleLength);
                        cycleCrossOver.add(Calendar.SECOND, cycleLength);
                        cycleEnd.add(Calendar.SECOND, cycleLength);
                    }
                } else // not cyclingFlag
                {
                    PSPControl.prepare();
                    // PSPControl.queryState();
                    PSPControl.outputOn();
                    PSPControl.queryState();
                }
            }
            System.out.println("Waiting until: " + formatter.format(gc.getTime()));
            waitUntil(gc.getTime());
        }
    }

    /**
     * Wait until a given Date.
     */
    void waitUntil(Date date) {
        final Object o = new Object();
        TimerTask tt = new TimerTask() {
            public void run() {
                synchronized (o) {
                    o.notify();
                }
            }
        };
        timer.schedule(tt, date);
        synchronized (o) {
            try {
                o.wait();
            } catch (InterruptedException ie) {
            }
        }
        timer.purge();
    }

    /**
     * @param args first command line argument should be name of properties file
     * from which to read serial port to open and start and stop times. Default
     * value is used if omitted.
     */
    public static void main(String[] args) {
        String propFileName;
        if (args.length > 0) {
            propFileName = args[0];
        } else {
            propFileName = "data\\PSP.properties";
        }
        (new PSPClock()).runPSPControl(propFileName);

    }
}