package aphalo.pspcontrol;

/*
 * PSPControl.java
 *
 * (based on PSPTerm)
 *
 * Created on 6 June 2009
 * Revised branch cretaed 10 June 2009
 *
 * Helper class to communicate with the programable PSU <em>PSP-2010</em> from <em>GW Instek</em>
 * through an RS-232 serial port.
 *
 * usage: java PSPTerm [<em>port</em>]
 *
 * bug: write method does not lock SerialWriter from echoing user input to PSP
 * 
 */

import gnu.io.CommPort;
import gnu.io.CommPortIdentifier;
import gnu.io.SerialPort;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DecimalFormat;
import java.util.Enumeration;
import java.util.concurrent.*;



/**
 * Class with methods to communicate with PSP-2010 power source (GW Instek)
 * through RS-232 port. (<emph>IT REQUIRES SPECIAL CABLE, SEE MANUAL</emph>).
 *
 * @author Pedro J. Aphalo, University of Helsinki
 * @version 1.1
 *
 * <a href="http://www.helsinki.fi/people/pedro.aphalo">
 *
 */

public class PSPControl {
    static CommPortIdentifier portId;
    static Enumeration portList;
    static OutputStream PSPout = System.out;
    
    private final static long PSPdelay = 500L; // half a second
    
    /** Creates a new instance of PSPControl. */
    public PSPControl() {
        super();
    }
           
    /** Write a character to the PSP. */
    public static void write(int i) {
        try {PSPout.write(i);}
        catch (IOException ioe) {
            System.out.println("Exception when writing to PSP");
        }
    }

    /** Commands to control and query the PSP power source */

    /** Send a <strong>cr</strong> to the PSP to flush any previous characters sent. */
    @SuppressWarnings("empty-statement")
    public static void prepare() {
        try {
            PSPout.write('\r');
            try{Thread.sleep(PSPdelay);}catch(InterruptedException ignore){};
        }
        catch (IOException ioe) {
            System.out.println("Exception when writing to PSP");
        }
    }

    /** Query PSP for current state and settings. */
    @SuppressWarnings("empty-statement")
     public static void queryState() {
        try {PSPout.write('L');
             PSPout.write('\r');
             try{Thread.sleep(PSPdelay);}catch(InterruptedException ignore){};
        }
        catch (IOException ioe) {
            System.out.println("Exception when writing to PSP");
        }
    }

    /** Set PSP relay status to ON. */
    @SuppressWarnings("empty-statement")
    public static void outputOn() {
        try {PSPout.write('K');
             PSPout.write('O');
             PSPout.write('E');
             PSPout.write('\r');
             try{Thread.sleep(PSPdelay);}catch(InterruptedException ignore){};
        }
        catch (IOException ioe) {
            System.out.println("Exception when writing to PSP");
        }
    }

    /** Set PSP relay status to OFF. */
    @SuppressWarnings("empty-statement")
    public static void outputOff() {
        try {PSPout.write('K');
             PSPout.write('O');
             PSPout.write('D');
             PSPout.write('\r');
             try{Thread.sleep(PSPdelay);}catch(InterruptedException ignore){};
        }
        catch (IOException ioe) {
            System.out.println("Exception when writing to PSP");
        }
    }
    
    /** Set PSP output voltage **/
    @SuppressWarnings("empty-statement")
    public static void voltageSet(float V) {
        String Vs = new DecimalFormat("00.00").format(V);
        try {PSPout.write('S');
             PSPout.write('V');
             PSPout.write(' ');
             for (int i = 0; i < 5; i = i+1) {
             PSPout.write(Vs.charAt(i));
             }
             PSPout.write('\r');
             try{Thread.sleep(PSPdelay);}catch(InterruptedException ignore){};
        }
        catch (IOException ioe) {
            System.out.println("Exception when writing to PSP");
        }
    }

    /** Set PSP output voltage limit **/
    @SuppressWarnings("empty-statement")
    public static void voltageLimitSet(float V) {
        String Vs = new DecimalFormat("00").format(V);
        try {PSPout.write('S');
             PSPout.write('U');
             PSPout.write(' ');
             for (int i = 0; i < 2; i = i+1) {
             PSPout.write(Vs.charAt(i));
             }
             PSPout.write('\r');
             try{Thread.sleep(PSPdelay);}catch(InterruptedException ignore){};
        }
        catch (IOException ioe) {
            System.out.println("Exception when writing to PSP");
        }
    }

    /** Set PSP output current limit **/
    @SuppressWarnings("empty-statement")
    public static void currentLimitSet(float A) {
        String As = new DecimalFormat("0.00").format(A);
        try {PSPout.write('S');
             PSPout.write('I');
             PSPout.write(' ');
             for (int i = 0; i < 4; i = i+1) {
             PSPout.write(As.charAt(i));
             }
             PSPout.write('\r');
             try{Thread.sleep(PSPdelay);}catch(InterruptedException ignore){};
        }
        catch (IOException ioe) {
            System.out.println("Exception when writing to PSP");
        }
    }

   /** Opens serial port and creates reading and writing processes. */
    void connect( String portName ) throws Exception {
        boolean portFound = false;
        System.out.println("Building portList...");
        portList = CommPortIdentifier.getPortIdentifiers();
        System.out.println("Analyzing portList...");
        while (portList.hasMoreElements()) {
            portId = (CommPortIdentifier) portList.nextElement();
            System.out.println(portId.getName());
            if (portId.getPortType() == CommPortIdentifier.PORT_SERIAL) {
                if (portId.getName().equals(portName)) {
                    System.out.println("Found port: " + portName);
                    portFound = true;
                }
            }
        }
        if (!portFound) {
            System.out.println("Port not found, exiting");
            System.exit(2); // replace with throw!
        }
        CommPortIdentifier portIdentifier = CommPortIdentifier.getPortIdentifier(portName);
        if ( portIdentifier.isCurrentlyOwned() ) {
            System.out.println("Error: Port is currently in use");
        } else {
            System.out.println("Opening port...");
            CommPort commPort = portIdentifier.open(this.getClass().getName(),2000);
            System.out.println("Port opened by " + this.getClass().getName());
            
            if ( commPort instanceof SerialPort ) {
                SerialPort serialPort = (SerialPort) commPort;
                serialPort.setSerialPortParams(2400,SerialPort.DATABITS_8,SerialPort.STOPBITS_1,SerialPort.PARITY_NONE);
                serialPort.setFlowControlMode(SerialPort.FLOWCONTROL_NONE);
                serialPort.setDTR(true);
                serialPort.setRTS(true);
                System.out.println("Port params set");
                
                InputStream in = serialPort.getInputStream();
                PSPout = serialPort.getOutputStream();
                
                System.out.println("Creating threads...");
                (new Thread(new SerialReader(in))).start();
                (new Thread(new SerialWriter(PSPout))).start();
                
            } else {
                System.out.println("Error: Only serial ports are handled by this example. Should never get here!");
                System.exit(2);
            }
        }
    }
    
    /** Reads from serial port and echoes to System.out. */
    public static class SerialReader implements Runnable {
        InputStream in;
        
        public SerialReader( InputStream in ) {
            this.in = in;
        }
        
        @Override
        public void run() {
            byte[] buffer = new byte[1024];
            int len = -1;
            try {
                while ( ( len = this.in.read(buffer)) > -1 ) {
                    System.out.print(new String(buffer,0,len));
                }
            } catch ( IOException e ) {
                e.printStackTrace();
            }
            Thread.yield();
        }
    }
    
    /** Reads from System.in and echoes to serial port 
      * ignoring comments and handling special commands.  */
    public static class SerialWriter implements Runnable {
        OutputStream out;

        public SerialWriter( OutputStream out ) {
            this.out = out;
        }

        @Override
        @SuppressWarnings("empty-statement")
        public void run() {
            try {
                boolean listening = false; // by default keyboard echoing to PSP is disabled
                int c = 0;
                while ( ( c = System.in.read()) > -1) {
                    if (c == '$'){
                        listening = !listening;
                        if (!listening){
                            System.out.println("Not listening to keyboard: type '$' to enable");
                        }
                    }
                    if (c == '@'){
                        System.out.println("Normal exit at user's request: switch OFF");
                        prepare();
                        outputOff();
                        System.exit(0); // if in a nested include pop back to outer file
                    }
                    if (c == '!'){
                        System.out.println("Normal exit at user's request: switch as is");
                        System.exit(0); // if in a nested include pop back to outer file
                    }
                    if (listening){
                        if (c == '*'){ //parse and execute local command
                            // for the time being echo and ignore!
                            do {
                                System.out.write(c);
                            }
                            while ( (c = System.in.read()) != '\n' );
                            System.out.write(c);
                        }
                        if (c == '#'){ //skip comment while copying it to System.out
                            do {
                                System.out.write(c);
                            }
                            while ( (c = System.in.read()) != '\n' );
                            System.out.write(c);
                        } else{
                            this.out.write(c);
                            if (c == '\n') {
                                //sleep for PSPdelay seconds to give enough time for the PSU to respond
                                try{Thread.sleep(PSPdelay);}catch(InterruptedException ignore){};
                            }
                        }
                    }
                    Thread.yield();
                }
            } catch ( IOException e ) {
                e.printStackTrace();
            }
        }
    }

   
}
